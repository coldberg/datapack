#include <cassert>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <cstddef>
#include <iterator>
#include <experimental/filesystem>

namespace stdfs = std::experimental::filesystem;

typedef std::uint64_t signature_type;
static const signature_type MAGIC_SIGNATUE = 0x524448434947414dull;

template <typename T>
auto read (std::istream& is) {
    T value;
    if (!is.read (reinterpret_cast<char*>(&value), sizeof (value))) {
        throw std::runtime_error (__FUNCSIG__ " : Unable to read value at offset " + std::to_string ((std::uintmax_t)is.tellg ()));
    }
    return value;
}

template <typename T>
auto write (std::ostream& os, const T& value) {
    if (!os.write (reinterpret_cast<const char*>(&value), sizeof (value))){
        throw std::runtime_error (__FUNCSIG__ " : Unable to write value at offset " + std::to_string ((std::uintmax_t)os.tellp ()));	
    }
}

void copy_bytes(std::istream& is, std::ostream& os, std::uintmax_t size) {
    std::copy_n (std::istreambuf_iterator<char> (is), size, 
                 std::ostreambuf_iterator<char> (os));
}
void copy_bytes(std::istream& is, std::ostream& os) {
    std::copy (std::istreambuf_iterator<char> (is), 
               std::istreambuf_iterator<char> (), 
               std::ostreambuf_iterator<char> (os));
}

auto write_string (std::ostream& os, const std::string& str) {
    write (os, str.length ());
    std::copy (str.begin (), str.end (), std::ostreambuf_iterator<char> (os));
}

auto read_string (std::istream& is) {	
    typedef decltype(std::string().length ()) length_type;

    auto length = read<length_type> (is);
    auto tmpstr = std::string {};
    std::copy_n  (std::istreambuf_iterator<char> (is), length, std::back_inserter (tmpstr));
    is.seekg (1u, std::ios::cur);
    return tmpstr;
}

template <typename T0, typename T1>
auto is_either(const T0& v0, const T1& v1) {
    return v0 == v1;
}

template <typename T0, typename T1, typename... Tn>
auto is_either(const T0& v0, const T1& v1, const Tn&... vn) {
    return v0 == v1 || is_either (v0, vn...);
}

bool seek_to_data_section (std::istream& is) {
    is.seekg(-int (sizeof (MAGIC_SIGNATUE) + sizeof(std::uintmax_t)), std::ios::end);
    auto data_offset = read<std::uintmax_t> (is);
    auto magic_signature = read<signature_type> (is);
    if (magic_signature != MAGIC_SIGNATUE) {
        is.seekg (0u, std::ios::beg);
        return false;
    }
    is.seekg (data_offset, std::ios::beg);
    return true;
}


int main (int argc, char** argv) try {
    std::ifstream self (argv[0], std::ios::binary);
    if (seek_to_data_section (self)) {
        const auto payload_name = read_string (self);
        const auto payload_size = read<std::uintmax_t> (self);
        std::ofstream payload_stream (payload_name, std::ios::binary);		
        copy_bytes (self, payload_stream, payload_size);
        system (("explorer "+payload_name).c_str ());
        return 0;
    }
    if (argc > 2) {
        
        std::ofstream output_stream (argv [2], std::ios::binary);
        std::ifstream payload_stream (argv [1], std::ios::binary);

        const auto payload_size = stdfs::file_size (argv[1]);
        const auto payload_name = stdfs::path(argv[1]).filename ().string ();

        const auto selfexe_size = stdfs::file_size (argv [0]);

        self.seekg (0u, std::ios::beg);
        copy_bytes (self, output_stream);
        write_string (output_stream, payload_name);
        write<std::uintmax_t> (output_stream, payload_size);
        copy_bytes (payload_stream, output_stream, payload_size);
        write<std::uintmax_t> (output_stream, selfexe_size);
        write<signature_type> (output_stream, MAGIC_SIGNATUE);
        return 0;
    }

    std::cout << "Usage : " << stdfs::path(argv[0]).filename().string() << " [payload] [output]\n";
    return -1;
}
catch (const std::exception& ex) {
    std::cout << ex.what () << "\n";
}
